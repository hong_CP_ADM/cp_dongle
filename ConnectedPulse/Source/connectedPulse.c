/**************************************************************************************************
  Filename:       dongle.c

  Revised:        $Date: 2011-12-16 15:46:52 -0800 (Fri, 16 Dec 2011) $
  Revision:       $Revision: 58 $

  Description:    This file contains the Dongle Sensor sample application
                  for use with the CC2540 Bluetooth Low Energy Protocol Stack.

  Copyright 2011-2013 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/


/*********************************************************************
 * INCLUDES
 */

#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "OnBoard.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "hal_lcd.h"
#include "hal_key.h"
#include "gatt.h"
#include "hci.h"
#include "hal_uart.h"
#include "gapgattserver.h"
#include "gattservapp.h"
#include "linkdb.h"
#include "peripheral.h"
#include "gapbondmgr.h"
#include "connectedPulseService.h"
#include "devinfoservice.h"
#include "connectedPulse.h"
#include "OSAL_Clock.h"
#include "osal_snv.h"

#if defined FEATURE_OAD
#include "oad.h"
#include "oad_target.h"
#endif

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

// Fast advertising interval in 625us units
#define DEFAULT_FAST_ADV_INTERVAL             32

// Duration of fast advertising duration in sec
#define DEFAULT_FAST_ADV_DURATION             30

// Slow advertising interval in 625us units
#define DEFAULT_SLOW_ADV_INTERVAL             1600

// Duration of slow advertising duration in sec
#define DEFAULT_SLOW_ADV_DURATION             30

// Whether to enable automatic parameter update request when a connection is formed
//#define DEFAULT_ENABLE_UPDATE_REQUEST       FALSE
#define DEFAULT_ENABLE_UPDATE_REQUEST         TRUE

// Minimum connection interval (units of 1.25ms) if automatic parameter update request is enabled
//#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     200
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     16

// Maximum connection interval (units of 1.25ms) if automatic parameter update request is enabled
//#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     1600
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     40

// Slave latency to use if automatic parameter update request is enabled
//#define DEFAULT_DESIRED_SLAVE_LATENCY         1
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms) if automatic parameter update request is enabled
//#define DEFAULT_DESIRED_CONN_TIMEOUT          1000
#define DEFAULT_DESIRED_CONN_TIMEOUT          600

// Default passcode
#define DEFAULT_PASSCODE                      19655

// Default GAP pairing mode
#define DEFAULT_PAIRING_MODE                  GAPBOND_PAIRING_MODE_INITIATE 

// Default MITM mode (TRUE to require passcode or OOB when pairing)
#define DEFAULT_MITM_MODE                     TRUE //FALSE

// Default bonding mode, TRUE to bond
#define DEFAULT_BONDING_MODE                  TRUE

// Default GAP bonding I/O capabilities
#define DEFAULT_IO_CAPABILITIES               GAPBOND_IO_CAP_DISPLAY_ONLY //GAPBOND_IO_CAP_NO_INPUT_NO_OUTPUT

// Notification period in ms
#define DEFAULT_NOTI_PERIOD                   100

// UART RX Data notification in ms
#define UART_RX_DATA_PERIOD                   10
   
// Meas state bit field
#define DONGLE_MEAS_STATE_VALID              0x01
#define DONGLE_MEAS_STATE_FILTER_PASS        0x02
#define DONGLE_MEAS_STATE_ALL                (DONGLE_MEAS_STATE_VALID | DONGLE_MEAS_STATE_FILTER_PASS)

// Some values used to simulate measurements
#define MEAS_IDX_MAX                          sizeof(dongleMeasArray)/sizeof(dongleMeas_t)

// Maximum number of dynamically allocated measurements (must be less than MEAS_IDX_MAX)
#define DYNAMIC_REC_MAX                       3

/*********************************************************************
 * TYPEDEFS
 */

// contains the data of control point command
typedef struct {
  osal_event_hdr_t hdr; //!< MSG_EVENT and status
  uint8 len;
  uint8 data[DONGLE_CTL_PNT_MAX_SIZE];
} dongleCtlPntMsg_t;


// Data in a dongle measurement as defined in the profile
typedef struct {
  uint8 state;
  uint8 flags;
  uint16 seqNum;
  UTCTimeStruct baseTime;
  int16 timeOffset;
  uint16 concentration;
  uint8 typeSampleLocation;
  uint16 sensorStatus;
} dongleMeas_t;

// Context data as defined in profile
typedef struct {
  uint8 flags;
  uint16 seqNum;
  uint8 extendedFlags;
  uint8 carboId;
  uint16 carboVal;
  uint8 mealVal;
  uint8 TesterHealthVal;
  uint16 exerciseDuration;
  uint8 exerciseIntensity;
  uint8 medId;
  uint16 medVal;
  uint16 HbA1cVal;
} dongleContext_t;

/*********************************************************************
 * GLOBAL VARIABLES
 */

// Task ID
uint8 dongleTaskId;

// Task ID
uint8 bridgeTaskId;

static int tick1 = 100;
static int tick2 = 20;

// Connection handle
uint16 gapConnHandle;

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

// GAP State
static gaprole_States_t gapProfileState = GAPROLE_INIT;

// GAP Profile - Name attribute for SCAN RSP data
static uint8 scanData[] =
{
  0x08,   // length of this data
  0x09,   // AD Type = Complete local name
  'D',
  'O',
  'N',
  'G',
  'L',
  'E'
};

static uint8 advertData[] =
{
  // flags
  0x02,
  GAP_ADTYPE_FLAGS,
  GAP_ADTYPE_FLAGS_LIMITED | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,
  // service UUIDs
  0x05,
  GAP_ADTYPE_16BIT_MORE,
  LO_UINT16(DONGLE_SERV_UUID),
  HI_UINT16(DONGLE_SERV_UUID),
  LO_UINT16(DEVINFO_SERV_UUID),
  HI_UINT16(DEVINFO_SERV_UUID)
};

// Device name attribute value
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = CONNECT_PUSLE;

// Bonded state
static bool dongleBonded = FALSE;

// Bonded peer address
static uint8 dongleBondedAddr[B_ADDR_LEN];

// GAP connection handle
static uint16 gapConnHandle;

// Indication structures for dongle
static attHandleValueNoti_t  dongleMeas;

static attHandleValueNoti_t dongleContext;

static attHandleValueInd_t dongleCtlPntRsp;

// Advertising user-cancelled state
static bool dongleAdvCancelled = FALSE;

//If true, then send all valid and selected dongle measurements
uint8 dongleSendAllRecords = 0;
uint8 dongleReceiveAllRecords = 0;
static uint8 LastSendNull = 0;
static uint8 serialNum = 0;
static uint8 ifSerialNum = 0;
static uint8 ifDual = 0;

/*********************************************************************
 * LOCAL FUNCTIONS
 */

static void dongle_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void dongleProcessCtlPntMsg( dongleCtlPntMsg_t* pMsg);
static void dongle_HandleKeys( uint8 shift, uint8 keys );
static void dongleGapStateCB( gaprole_States_t newState );
static void dongleCB(uint8 event, uint8* valueP, uint8 len);
static void donglePasscodeCB( uint8 *deviceAddr, uint16 connectionHandle,
                                        uint8 uiInputs, uint8 uiOutputs );
static void donglePairStateCB( uint16 connHandle, uint8 state, uint8 status );

static void dongleCtlPntResponse(uint8 rspCode, uint8 opcode);
static void dongleCtlPntHandleOpcode(uint8 opcode, uint8 oper, uint8 filterType, void* param1, void* param2);
static void dongleDataSend(uint8 *string, uint8 length, uint8 sendNext);
static void dongleMeasSend(uint8 *string, uint8 length, uint8 sendNext, uint8 serial);
static void dongleContextSend(uint8 *string, uint8 length, uint8 sendNext, uint8 serial);
static void dongleSendNext(uint8 length);
static void dongleReceiveNext(void);

/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t dongle_PeripheralCBs =
{
  dongleGapStateCB,  // Profile State Change Callbacks
  NULL                // When a valid RSSI is read from controller
};

// Bond Manager Callbacks
static const gapBondCBs_t dongleBondCB =
{
  donglePasscodeCB,
  donglePairStateCB
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */
#define UART_RX_BUF_SIZE 500
uint8 uartRxBuf[UART_RX_BUF_SIZE+1];
uint16 uartRxWrite = 0;
uint16 uartRxRead = 0;
uint8 dongleSendLen = ATT_MTU_SIZE-4;

void connCBack(uint8 port, uint8 event)
{   

#if 1
  if(event & HAL_UART_RX_START)
  {
    event &= (~HAL_UART_RX_START);
    if(dongleReceiveAllRecords == 0)
    {  
       osal_start_timerEx(dongleTaskId, UART_RX_DATA_EVT, UART_RX_DATA_PERIOD);  
       dongleReceiveAllRecords = 2;
    }    
  } 
#endif
  
  switch (event) {         
      case HAL_UART_RX_FULL:    

#if (HAL_LCD == TRUE)         
          HalLcdWriteString( "HAL_UART_RX_FULL", HAL_LCD_LINE_2);  
#endif                    
          dongleReceiveNext( );
          
          break;
          
       case HAL_UART_RX_ABOUT_FULL:    
          
          dongleReceiveNext( );

#if (HAL_LCD == TRUE)         
          HalLcdWriteString( "HAL_UART_RX_ABOUT_FULL", HAL_LCD_LINE_2 );  
#endif                          
          break;
          
       case HAL_UART_RX_TIMEOUT:    
           //HalLcdWriteString( "HAl_UART_RX_TIMEOUT", HAL_LCD_LINE_2 );    
           break;

       case HAL_UART_TX_FULL:    
           //HalLcdWriteString( "HAl_UART_TX_FULL", HAL_LCD_LINE_2);    
           break;

       case HAL_UART_TX_EMPTY:    
           //HalLcdWriteString( "HAl_UART_TX_EMPTY", HAL_LCD_LINE_2);    
           break;
           
       default:
           //HalLcdWriteStringValue( "default:", (uint16)(event), 10,  HAL_LCD_LINE_2 );
           break;            
   }         
   
}

uint16 uartBaudrate[5] = {96, 192,384,576, 1152};
void ConnectedPulseInit(uint32 baud, uint8 startbit, uint8 parity, uint8 stopbit, uint8 flow)
{
  /* UART Test */
  HalUARTInit( );

  halUARTCfg_t uartConfig;  
  
  // configure UART
  uartConfig.configured           = TRUE;
  uartConfig.baudRate             = baud;
  uartConfig.flowControl          = flow;
  uartConfig.flowControlThreshold = 48;
  uartConfig.parityControl        = parity;
  uartConfig.rx.maxBufSize        = 512;
  uartConfig.tx.maxBufSize        = 32;
  uartConfig.idleTimeout          = 6;
  uartConfig.intEnable            = TRUE;
  uartConfig.callBackFunc         = (halUARTCBack_t)connCBack;

  // Note: Assumes no issue opening UART port.
#if (HAL_LCD == TRUE)  
  HalLcdWriteStringValue( "Baud",  uartBaudrate[baud], 10, HAL_LCD_LINE_2 );
  HalLcdWriteStringValue( "FlowControl",  flow, 10, HAL_LCD_LINE_3 );
#endif
  
  HalUARTOpen(HAL_UART_PORT_0,  &uartConfig);    
}     

/*********************************************************************
 * @fn      Dongle_Init
 *
 * @brief   Initialization function for the Dongle App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void Dongle_Init( uint8 task_id )
{
  dongleTaskId = task_id;
  uint8 status;
  
  // Setup the GAP Peripheral Role Profile
  {
    // For other hardware platforms, device starts advertising upon initialization
    uint8 initial_advertising_enable = FALSE;
    
    // By setting this to zero, the device will go into the waiting state after
    // being discoverable for 30.72 second, and will not being advertising again
    // until the enabler is set back to TRUE
    uint16 gapRole_AdvertOffTime = 0;

    uint8 enable_update_request = DEFAULT_ENABLE_UPDATE_REQUEST;
    uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
    uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
    uint16 desired_slave_latency = DEFAULT_DESIRED_SLAVE_LATENCY;
    uint16 desired_conn_timeout = DEFAULT_DESIRED_CONN_TIMEOUT;

    // Set the GAP Role Parameters         
    GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );   
    GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME, sizeof( uint16 ), &gapRole_AdvertOffTime );
    GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanData ), scanData );
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertData ), advertData );

    GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE, sizeof( uint8 ), &enable_update_request );
    GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );
    GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );
    GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY, sizeof( uint16 ), &desired_slave_latency );
    GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER, sizeof( uint16 ), &desired_conn_timeout );
       
  }

  // Set the GAP Characteristics
  GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );

  // Setup the GAP Bond Manager
  {
    uint32 passkey = DEFAULT_PASSCODE;
    uint8 pairMode = DEFAULT_PAIRING_MODE;
    uint8 mitm = DEFAULT_MITM_MODE;
    uint8 ioCap = DEFAULT_IO_CAPABILITIES;
    uint8 bonding = DEFAULT_BONDING_MODE;

    GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
    GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
    GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
    GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
    GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
  }

  // Initialize GATT Client
  VOID GATT_InitClient();

  // Register to receive incoming ATT Indications/Notifications
  GATT_RegisterForInd( dongleTaskId );

  // Initialize GATT attributes
  GGS_AddService( GATT_ALL_SERVICES );         // GAP
  GATTServApp_AddService( GATT_ALL_SERVICES ); // GATT attributes
  Dongle_AddService( GATT_ALL_SERVICES );

#if defined FEATURE_OAD 
  OADTarget_AddService(); // OAD Profile
#endif
  
  DevInfo_AddService( );

  // Register for Dongle service callback
  Dongle_Register ( dongleCB);

  // Register for all key events - This app will handle all key events
  RegisterForKeys( dongleTaskId );

 #if defined ( CC2540_MINIDK ) 
  
  // makes sure LEDs are off
  HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );

  // For keyfob board set GPIO pins into a power-optimized state
  // Note that there is still some leakage current from the buzzer,
  // accelerometer, LEDs, and buttons on the PCB.

  P0SEL = 0; // Configure Port 0 as GPIO
  P1SEL = 0; // Configure Port 1 as GPIO
  P2SEL = 0; // Configure Port 2 as GPIO

  P0DIR = 0xFC; // Port 0 pins P0.0 and P0.1 as input (buttons),
                // all others (P0.2-P0.7) as output
  P1DIR = 0xFF; // All port 1 pins (P1.0-P1.7) as output
  P2DIR = 0x1F; // All port 1 pins (P2.0-P2.4) as output

  P0 = 0x03; // All pins on port 0 to low except for P0.0 and P0.1 (buttons)
  P1 = 0;   // All pins on port 1 to low
  P2 = 0;   // All pins on port 2 to low

#endif // #if defined( CC2540_MINIDK )

#if (HAL_LCD == TRUE)
  HalLcdWriteString( "Dongle", HAL_LCD_LINE_1 );
#endif
  
  ConnectedPulseInit(0, 8, 0, 1, 0);

  // Initialize NVRAM
  osal_snv_init( );
     
  // Setup a delayed profile startup
  osal_set_event( dongleTaskId, START_DEVICE_EVT );
  
  // Set fast advertising interval for user-initiated connections
  GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, DEFAULT_FAST_ADV_INTERVAL );
  GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, DEFAULT_FAST_ADV_INTERVAL );
  GAP_SetParamValue( TGAP_LIM_ADV_TIMEOUT, DEFAULT_FAST_ADV_DURATION );

  // toggle GAP advertisement status
  GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &status );
  status = !status;
  GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &status );

  dongleAdvCancelled = FALSE;      
        
}

/*********************************************************************
 * @fn      Dongle_ProcessEvent
 *
 * @brief   Dongle Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 Dongle_ProcessEvent( uint8 task_id, uint16 events )
{

  VOID task_id; // OSAL required parameter that isn't used in this function

  if ( events & SYS_EVENT_MSG )
  {
    uint8 *pMsg;

    if ( (pMsg = osal_msg_receive( dongleTaskId )) != NULL )
    {
      dongle_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

      // Release the OSAL message
      VOID osal_msg_deallocate( pMsg );
    }

    // return unprocessed events
    return (events ^ SYS_EVENT_MSG);
  }

  if ( events & START_DEVICE_EVT )
  {
    // Start the Device
    VOID GAPRole_StartDevice( &dongle_PeripheralCBs );

    // Register with bond manager after starting device
    GAPBondMgr_Register( (gapBondCBs_t *) &dongleBondCB );
    
    HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);

    return ( events ^ START_DEVICE_EVT );
  }

  if ( events & UART_RX_DATA_EVT )
  {
    // Send next notification
    dongleReceiveNext( );
    
    return ( events ^ NOTI_TIMEOUT_EVT );
  }
  
  return 0;
}

/*********************************************************************
 * @fn      dongle_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void dongle_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
  switch ( pMsg->event )
  {
  case KEY_CHANGE:
      dongle_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
      break;

  case CTL_PNT_MSG:
      dongleProcessCtlPntMsg( (dongleCtlPntMsg_t *) pMsg);
      break;

  default:
      break;
  }
}

/*********************************************************************
 * @fn      dongle_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
static void dongle_HandleKeys( uint8 shift, uint8 keys )
{
  uint32 ret;
  
  if ( keys & HAL_KEY_SW_1 )
  {    
    if(!dongleSendAllRecords)
    {
      if( gapProfileState == GAPROLE_CONNECTED)
      {
          if(uartRxWrite > 0)
          {  
            dongleMeasSend(uartRxBuf, uartRxWrite, DEFAULT_NOTI_PERIOD, 0);
            uartRxWrite = 0;
          }
          else 
          {
            ret = HalUARTRead(HAL_UART_PORT_0, uartRxBuf, UART_RX_BUF_SIZE);
            if(ret){
               dongleMeasSend(uartRxBuf, ret, DEFAULT_NOTI_PERIOD, 0);
            }
          }  
      }
    }
  }

  if ( keys & HAL_KEY_SW_2 )
  {
    // if device is not in a connection, pressing the right key should toggle
    // advertising on and off
    if( gapProfileState != GAPROLE_CONNECTED )
    {
      uint8 status;

      // Set fast advertising interval for user-initiated connections
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, DEFAULT_FAST_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, DEFAULT_FAST_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_ADV_TIMEOUT, DEFAULT_FAST_ADV_DURATION );

      // toggle GAP advertisement status
      GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &status );
      status = !status;
      GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &status );

      // Set state variable
      if (status == FALSE)
      {
#if (HAL_LCD == TRUE)        
        HalLcdWriteString( "Stopped", HAL_LCD_LINE_3 );
#endif
        dongleAdvCancelled = TRUE;       
      }
      else {
        dongleAdvCancelled = FALSE; 
#if (HAL_LCD == TRUE)        
        HalLcdWriteString( "Advertising ...", HAL_LCD_LINE_3 );   
#endif        
      }
    }
  }
}

/*********************************************************************
 * @fn      dongleGapStateCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void dongleGapStateCB( gaprole_States_t newState )
{  
  // if connected
  if ( newState == GAPROLE_CONNECTED )
  {
#if (HAL_LCD == TRUE)    
     HalLcdWriteString( "CONNECTED", HAL_LCD_LINE_3 );
#endif
     
     HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
  }
  // if disconnected
  else if (gapProfileState == GAPROLE_CONNECTED &&
           newState != GAPROLE_CONNECTED)
  {
    uint8 advState = TRUE;

#if (HAL_LCD == TRUE)    
    HalLcdWriteStringValue( "DISCONNECTED", newState, 10, HAL_LCD_LINE_3 );
#endif
    // clear state variables
    dongleReceiveAllRecords = 0;
    dongleSendAllRecords = 0;
         
    // stop notification timer
    osal_stop_timerEx(dongleTaskId, NOTI_TIMEOUT_EVT);

    if ( newState == GAPROLE_WAITING_AFTER_TIMEOUT )
    {
      // link loss timeout-- use fast advertising
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, DEFAULT_FAST_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, DEFAULT_FAST_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_ADV_TIMEOUT, DEFAULT_FAST_ADV_DURATION );
    }
    else
    {
      // Else use slow advertising
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, DEFAULT_SLOW_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, DEFAULT_SLOW_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_ADV_TIMEOUT, DEFAULT_SLOW_ADV_DURATION );
    }

    // Enable advertising
    GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
  }
  // if advertising stopped
  else if ( gapProfileState == GAPROLE_ADVERTISING &&
            newState == GAPROLE_WAITING )
  { 
#if (HAL_LCD == TRUE)    
     HalLcdWriteString( "WAITING", HAL_LCD_LINE_3 );
#endif    
     
     HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);

    // if advertising stopped by user
    if ( dongleAdvCancelled )
    {
      dongleAdvCancelled = FALSE;
    }
    // if fast advertising switch to slow
    else if ( GAP_GetParamValue( TGAP_LIM_DISC_ADV_INT_MIN ) == DEFAULT_FAST_ADV_INTERVAL )
    {      
      uint8 advState = TRUE;
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, DEFAULT_SLOW_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, DEFAULT_SLOW_ADV_INTERVAL );
      GAP_SetParamValue( TGAP_LIM_ADV_TIMEOUT, DEFAULT_SLOW_ADV_DURATION );
      GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
    }
  }
  // if started
  else if ( newState == GAPROLE_STARTED )
  {
#if (HAL_LCD == TRUE)  
    HalLcdWriteString( "STARTED", HAL_LCD_LINE_3 );
#endif
    // Set the system ID from the bd addr
    uint8 systemId[DEVINFO_SYSTEM_ID_LEN];
    GAPRole_GetParameter(GAPROLE_BD_ADDR, systemId);

    // shift three bytes up
    systemId[7] = systemId[5];
    systemId[6] = systemId[4];
    systemId[5] = systemId[3];

    // set middle bytes to zero
    systemId[4] = 0;
    systemId[3] = 0;

    DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);
  }
  else if( newState == GAPROLE_ADVERTISING )
  {
#if (HAL_LCD == TRUE)    
    HalLcdWriteString( "GADVERTISING", HAL_LCD_LINE_3 );  
#endif    
    uartRxWrite = uartRxRead = 0;
    HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
    HalLedSet (HAL_LED_2, HAL_LED_MODE_FLASH);
      
  }
  
  gapProfileState = newState;
}

/*********************************************************************
 * @fn      donglePairStateCB
 *
 * @brief   Pairing state callback.
 *
 * @return  none
 */
static void donglePairStateCB( uint16 connHandle, uint8 state, uint8 status )
{
  if ( state == GAPBOND_PAIRING_STATE_COMPLETE )
  {
    if ( status == SUCCESS )
    {
      linkDBItem_t  *pItem;

      if ( (pItem = linkDB_Find( gapConnHandle )) != NULL )
      {
        // Store bonding state of pairing
        dongleBonded = ( (pItem->stateFlags & LINK_BOUND) == LINK_BOUND );

        if ( dongleBonded )
        {
          osal_memcpy( dongleBondedAddr, pItem->addr, B_ADDR_LEN );
        }
      }
    }
  }
}

/*********************************************************************
 * @fn      donglePasscodeCB
 *
 * @brief   Passcode callback.
 *
 * @return  none
 */
static void donglePasscodeCB( uint8 *deviceAddr, uint16 connectionHandle,
                                        uint8 uiInputs, uint8 uiOutputs )
{
  // Send passcode response
  GAPBondMgr_PasscodeRsp( connectionHandle, SUCCESS, DEFAULT_PASSCODE );
}

/*********************************************************************
 * @fn      dongleCB
 *
 * @brief   Callback function for dongle service.
 *
 * @param   event - service event
 *
 * @return  none
 */
static void dongleCB(uint8 event, uint8* valueP, uint8 len)
{

  switch (event)
  {
  case DONGLE_CTL_PNT_CMD:
    {
      dongleCtlPntMsg_t* msgPtr;

      // Send the address to the task
      msgPtr = (dongleCtlPntMsg_t *)osal_msg_allocate( sizeof(dongleCtlPntMsg_t) );
      if ( msgPtr )
      {
        msgPtr->hdr.event = CTL_PNT_MSG;
        msgPtr->len = len;
        osal_memcpy(msgPtr->data, valueP, len);

        osal_msg_send( dongleTaskId, (uint8 *)msgPtr );
      }
    }
    break;

  default:
    break;
  }
}

/*********************************************************************
 * @fn      dongleProcessCtlPntMsg
 *
 * @brief   Process Control Point messages
 *
 * @return  none
 */
static void dongleProcessCtlPntMsg( dongleCtlPntMsg_t* pMsg)
{
  uint8 opcode = pMsg->data[0];
  uint8 oper = pMsg->data[1];
  uint16 len= pMsg->len;
  unsigned char  *buf;
           
  switch(opcode)
  {
      case CTL_PNT_OP_LED:
#if (HAL_LCD == TRUE)        
      	HalLcdWriteString( "LED", HAL_LCD_LINE_2 );
#endif        
      	buf = pMsg->data + 2;
        dongleCtlPntHandleOpcode(opcode, oper, len-2, buf, NULL);
        break;
        
      case CTL_PNT_OP_WRITE:
#if (HAL_LCD == TRUE)        
      	HalLcdWriteString( "WRITE", HAL_LCD_LINE_2 );
#endif        
      	buf = pMsg->data + 2;
      	        
        dongleCtlPntHandleOpcode(opcode, oper, len-2, buf, NULL);
        break;

      case CTL_PNT_OP_READ:
#if (HAL_LCD == TRUE)        
      	HalLcdWriteString( "READ", HAL_LCD_LINE_2 );
#endif  
        buf = pMsg->data + 2;      
        dongleCtlPntHandleOpcode(opcode, oper, len-2, buf, NULL);
        break;
        
      case CTL_PNT_NV_WRITE:
#if (HAL_LCD == TRUE)        
      	HalLcdWriteString( "NV-WRITE", HAL_LCD_LINE_2 );
#endif        
      	buf = pMsg->data + 2;
      	        
        dongleCtlPntHandleOpcode(opcode, oper, len-2, buf, NULL);
        break;

      case CTL_PNT_NV_READ:
#if (HAL_LCD == TRUE)        
      	HalLcdWriteString( "NV-READ", HAL_LCD_LINE_2 );
#endif        
        buf = pMsg->data + 2;
        dongleCtlPntHandleOpcode(opcode, oper, len-2, buf, NULL);
        break;
        
      case CTL_PNT_OP_CONFIG:    
#if (HAL_LCD == TRUE)        
        HalLcdWriteString( "CONFIG", HAL_LCD_LINE_2 );
#endif        
        buf = pMsg->data + 2;
        dongleCtlPntHandleOpcode(opcode, oper, len-2, buf, NULL);
        break;
 
      case CTL_PNT_OP_TIMER:
        buf = pMsg->data + 2;
        if(buf[0] != 0)
          tick1 = buf[0];
        
        if(buf[1] != 0)
          tick2 = buf[1]; 

        ifDual = buf[2];
        ifSerialNum = buf[3];
        
#if (HAL_LCD == TRUE)        
        HalLcdWriteString( "TIMER1", HAL_LCD_LINE_1 );
        HalLcdWriteStringValue( "RX TICK-1",  buf[0], 10, HAL_LCD_LINE_2 );
        HalLcdWriteStringValue( "TX TICK-2",  buf[1], 10, HAL_LCD_LINE_3 );     
#endif         
        dongleCtlPntResponse(CTL_PNT_RSP_SUCCESS, opcode);
        
        break;
        
      default:
#if (HAL_LCD == TRUE)             
        HalLcdWriteStringValue( "NOT SUPPORTED", oper, 10, HAL_LCD_LINE_2 );
#endif        
        dongleCtlPntResponse(CTL_PNT_RSP_OPER_NOT_SUPPORTED, opcode);
        break;
  }  
}

static void dongleDataSend(uint8 *buf, uint8 length, uint8 tick)
{
  if(length != 0)
  {  
    serialNum++;
  }
  
  if((serialNum%2)==1)
  {  
     if(ifDual)
     {  
       dongleContextSend(buf, length, tick, serialNum);
     }
     else
     {
       dongleMeasSend(buf, length, tick, serialNum);
     }  
  }
  else
  {
     dongleMeasSend(buf, length, tick, serialNum);
  }  
}

/*********************************************************************
 * @fn      dongleMeasSend
 *
 * @brief   Prepare and send a dongle measurement
 *
 * @return  none
 */
static void dongleContextSend(uint8 *buf, uint8 length, uint8 tick, uint8 serial)
{   
  // att value notification structure
  uint8 *p = dongleContext.value;
  
  if(length != 0)
  {
     LastSendNull = 0;
  }  
  
  if( length == 0)
  {
     if(LastSendNull>2) 
     { 
       dongleSendAllRecords = 0; 
       return;
     }  
     else
       LastSendNull++;
  }  
     
  *p++ = length;
  if(ifSerialNum)
  {  
    *p++ = serial;
  }  
  
  for(int i=0;i<length;i++){  	    
    *p++ = buf[i];
  }
  
  dongleContext.len = (uint8) (p - dongleContext.value);

  //Send Measurement
  if((length != 0) || (dongleSendAllRecords==0))   
  {  
    Dongle_ContextSend( gapConnHandle, &dongleContext,  dongleTaskId);
  }
  
  if((dongleSendAllRecords) && tick)
  {  
    osal_start_timerEx(bridgeTaskId, UART_RX_DATA_EVT, tick);  
  }  
}

/*********************************************************************
 * @fn      dongleMeasSend
 *
 * @brief   Prepare and send a dongle measurement
 *
 * @return  none
 */
static void dongleMeasSend(uint8 *buf, uint8 length, uint8 tick, uint8 serial)
{   
  // att value notification structure
  uint8 *p = dongleMeas.value;
  
  if(length != 0)
  {
     LastSendNull = 0;
  }  
  
  if( length == 0)
  {
     if(LastSendNull>2) 
     { 
       dongleSendAllRecords = 0; 
       return;
     }  
     else
       LastSendNull++;
  }  
     
  *p++ = length;
  
  if(ifSerialNum)
  {  
    *p++ = serial;
  }  
  
  for(int i=0;i<length;i++){  	    
    *p++ = buf[i];
  }
  
  dongleMeas.len = (uint8) (p - dongleMeas.value);

  //Send Measurement
  if((length != 0) || (dongleSendAllRecords==0))   
  {  
    Dongle_MeasSend( gapConnHandle, &dongleMeas,  dongleTaskId);
  }
  
  if((dongleSendAllRecords) && tick)
  {  
    osal_start_timerEx(bridgeTaskId, UART_RX_DATA_EVT, tick);  
  }  
}

static void dongleReceiveNext( )
{
  uint16 ret=1, count=0, receive=0;
  static uint16 last = 0;
  
  count = Hal_UART_RxBufLen( HAL_UART_PORT_0 );

  if((count != 0) && ((count > 19) || (count == last)))
  { 
    dongleReceiveAllRecords = 2; 
    while (ret)
    {     
      if( uartRxRead <= uartRxWrite ) 
      {  
        ret = HalUARTRead(HAL_UART_PORT_0, &uartRxBuf[uartRxWrite], UART_RX_BUF_SIZE-uartRxWrite);
        if(ret)
        {    
          receive++;
          uartRxWrite += ret;
    
          if((uartRxWrite - uartRxRead) > (UART_RX_BUF_SIZE-32))
          {
            ret = 0;   
          }
        
          if(uartRxWrite >= UART_RX_BUF_SIZE)
          {
            uartRxWrite = 0;  
          }       
        }       
      }
      else if(uartRxWrite < (uartRxRead-1)) 
      {
        ret = HalUARTRead(HAL_UART_PORT_0, &uartRxBuf[uartRxWrite], uartRxRead-uartRxWrite-1);
        if(ret)
        { 
          receive++; 
          uartRxWrite += ret;
        
          if((uartRxRead - uartRxWrite) < 32)
          {
            ret = 0;
          }  
        }         
      }
      else
      {
         ret = 0; 
      }
       
      if(receive>0)
      {
        dongleReceiveAllRecords = 2;
        break;
      }  
    }
  }
  else 
  {
     last = count; 
  }  

  if(receive == 0) 
  { 
      if(dongleReceiveAllRecords !=0 )
      {  
        dongleReceiveAllRecords--;
      }  
  }
  else if(dongleSendAllRecords == 0)
  {
      dongleSendAllRecords = 2;  
      osal_start_timerEx(bridgeTaskId, UART_RX_DATA_EVT, UART_RX_DATA_PERIOD);  
  } 
  
  if(dongleReceiveAllRecords)
  {  
    osal_start_timerEx(dongleTaskId, UART_RX_DATA_EVT, tick1);   
  }  
}

static void dongleSendNext(uint8 len)
{
  int sent=0;
  
  if(len > ATT_MTU_SIZE-5)
  {
    len = ATT_MTU_SIZE-5;
  }
  
  if(!ifSerialNum)
  {  
    len++;
  }
  
  while( uartRxRead != uartRxWrite)
  {  
    if(uartRxRead<uartRxWrite)
    {
      if(uartRxWrite-uartRxRead>len) 
      {
        dongleDataSend(uartRxBuf+uartRxRead, len, 0);
        uartRxRead += len;
        sent++;
      }
      else {
        dongleDataSend(uartRxBuf+uartRxRead, uartRxWrite-uartRxRead, 0);
        uartRxRead = uartRxWrite;
        sent++;
      }  
    }
    else if(uartRxRead>uartRxWrite)
    {
      if(UART_RX_BUF_SIZE-uartRxRead>len) 
      {
        dongleDataSend(uartRxBuf+uartRxRead, len, 0);
        uartRxRead += len;
        sent++;
      }
      else {
        dongleDataSend(uartRxBuf+uartRxRead, UART_RX_BUF_SIZE-uartRxRead, 0);
        uartRxRead = 0;        
        sent++;
      }
    }
    else
    {
      break;
    }  
        
    if(sent>0) 
    {
      dongleSendAllRecords = 2;
    }  
    
    if(sent>0)
    {  
      break;
    }      
  }
   
  if(sent == 0)  
  {
      dongleSendAllRecords--;     
      
      dongleDataSend(NULL, 0, DEFAULT_NOTI_PERIOD);   
      
      if(dongleSendAllRecords == 0)
      {
        HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);   
      }    
  }
  else
  {
       dongleDataSend(NULL, 0, tick2);  
  }  
}

/*********************************************************************
 * @fn      dongleCtlPntResponse
 *
 * @brief   Send a record control point response
 *
 * @param   rspCode - the status code of the operation
 * @param   opcode - control point opcode
 *
 * @return  none
 */
static void dongleCtlPntResponse(uint8 rspCode, uint8 opcode)
{
  dongleCtlPntRsp.len = 4;
  dongleCtlPntRsp.value[0] = CTL_PNT_OP_REQ_RSP;
  dongleCtlPntRsp.value[1] = CTL_PNT_OPER_NULL;
  dongleCtlPntRsp.value[2] = opcode;
  dongleCtlPntRsp.value[3] = rspCode;

  Dongle_CtlPntIndicate(gapConnHandle, &dongleCtlPntRsp,  dongleTaskId);
}

/*********************************************************************
 * @fn      dongleCtlPntHandleOpcode
 *
 * @brief   Mark all records earlier than a specific time
 *
 * @param   time - time filter
 * @param   set - if true bits are set, otherwise cleared
 * @param   mask - bits to set or clear
 *
 * @return  number of valid records set or cleared.
 */
static void dongleCtlPntHandleOpcode(uint8 opcode, uint8 oper, uint8 len, void* param1, void* param2)
{
  uint16 ret;
  uint8 *buf = param1;
  osalSnvId_t id;
  osalSnvLen_t length;
  uint8 length2;
  void *pBuf;
  static uint8 buffer[128];
      
  switch (opcode)
  {
    case CTL_PNT_OP_LED:      
      HalLedSet (buf[0], buf[1]);
      break;
      
    case CTL_PNT_OP_CONFIG:
      ConnectedPulseInit(buf[0], buf[1], buf[2], buf[3], buf[4]);
      dongleCtlPntResponse(CTL_PNT_RSP_SUCCESS, CTL_PNT_OP_CONFIG_RSP);       
      break;
      
    case CTL_PNT_OP_WRITE:   
      
      HalLedSet (HAL_LED_1, HAL_LED_MODE_FLASH);
      
      HalUARTWrite(HAL_UART_PORT_0, buf, len);
      dongleCtlPntResponse(CTL_PNT_RSP_SUCCESS, CTL_PNT_OP_WRITE_RSP); 
  
      break;    
      
    case CTL_PNT_OP_READ:
      
      if(oper == 1) {
        dongleReceiveAllRecords = 2;
      }
      else {
        dongleReceiveAllRecords = 1;
      }
              
      osal_start_timerEx(dongleTaskId, UART_RX_DATA_EVT, UART_RX_DATA_PERIOD);  
      
      dongleCtlPntResponse(CTL_PNT_RSP_SUCCESS, CTL_PNT_OP_READ_RSP); 
         
      break;

    case CTL_PNT_NV_WRITE:
      id = buf[0];
      length = buf[1];
      pBuf = &buf[2];
      ret = osal_snv_write( id, length, pBuf );
      if(ret == SUCCESS)
      {
#if (HAL_LCD == TRUE)        
      	HalLcdWriteStringValue( "NV-WR:", id, 10,  HAL_LCD_LINE_2 );
        HalLcd_HW_WriteLineValue(3, pBuf, length, 16);
#endif 
      }
      else
      {
#if (HAL_LCD == TRUE)        
      	HalLcdWriteStringValue( "NV-WR", id, 10, HAL_LCD_LINE_2 );
        HalLcdWriteStringValue("ERROR", ret, 10, HAL_LCD_LINE_3 );
#endif      
      }
         
      break; 

    case CTL_PNT_NV_READ:
      id = buf[0];
      length = buf[1];
      pBuf = buffer;      
      ret = osal_snv_read2( id,  length, pBuf, &length2);
      if(ret == SUCCESS) 
      {
#if (HAL_LCD == TRUE)        
      	HalLcdWriteStringValue( "NV-RD", id, 10, HAL_LCD_LINE_2 );
        HalLcd_HW_WriteLineValue(3, pBuf, length2, 16);
#endif        
        dongleMeasSend(pBuf, length, DEFAULT_NOTI_PERIOD, 0);
      }
      else 
      {
 #if (HAL_LCD == TRUE)        
      	HalLcdWriteStringValue( "NV-RD", id, 10, HAL_LCD_LINE_2 );
        HalLcdWriteStringValue("ERROR", ret, 10, HAL_LCD_LINE_3 );
#endif      
      }
      
      break;       
  }  
}

/*
 * Task Initialization for the Glucose Application
 */
void Bridge_Init( uint8 task_id )
{
  bridgeTaskId = task_id;
  
}

/*
 * Task Event Processor for the Glucose Application
 */
uint16 Bridge_ProcessEvent( uint8 task_id, uint16 events )
{
  VOID task_id; // OSAL required parameter that isn't used in this function

  if ( events & UART_RX_DATA_EVT )
  {
    // Send next notification
    dongleSendNext(dongleSendLen);
    
    return ( events ^ UART_RX_DATA_EVT );
  }

  return 0;
}

/*********************************************************************
*********************************************************************/
