/**************************************************************************************************
  Filename:       glucservice.h
  Revised:        $Date: 2011-12-16 15:46:52 -0800 (Fri, 16 Dec 2011) $
  Revision:       $Revision: 58 $

  Description:    This file contains the Dongle service definitions and
                  prototypes.

 Copyright 2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

#ifndef DONGLESERVICE_H
#define DONGLESERVICE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include "OSAL_clock.h"
  
/*********************************************************************
 * CONSTANTS
 */
  
// Dongle Service Parameters
#define DONGLE_FEATURE_PARAM             3

// Characteristic Value sizes
#define DONGLE_CTL_PNT_MIN_SIZE          2  
#define DONGLE_CTL_PNT_MAX_SIZE          20  

// Dongle Service UUIDs
#define DONGLE_SERV_UUID                 0x1808
#define DONGLE_MEAS_UUID                 0x2A18
#define DONGLE_CONTEXT_UUID              0x2A34
#define DONGLE_FEATURE_UUID              0x2A51
#define RECORD_CONTROL_POINT_UUID        0x2A52

// Values for meas flags

#define DONGLE_MEAS_FLAG_TIME_OFFSET           0x01
#define DONGLE_MEAS_FLAG_CONCENTRATION         0x02
#define DONGLE_MEAS_FLAG_UNITS                 0x04
#define DONGLE_MEAS_FLAG_STATUS_ANNUNCIATION   0x08
#define DONGLE_MEAS_FLAG_CONTEXT_INFO          0x10

#define DONGLE_MEAS_FLAG_ALL  (DONGLE_MEAS_FLAG_TIME_OFFSET | DONGLE_MEAS_FLAG_CONCENTRATION | DONGLE_MEAS_FLAG_UNITS | DONGLE_MEAS_FLAG_STATUS_ANNUNCIATION | DONGLE_MEAS_FLAG_CONTEXT_INFO)
  
// Values for Type Nibble
#define DONGLE_TYPE_CAPILLARY_WHOLE            0x1
#define DONGLE_TYPE_CAPILLARY_PLASMA           0x2
#define DONGLE_TYPE_VENOUS_WHOLE               0x3
#define DONGLE_TYPE_VENOUS_PLASMA              0x4
#define DONGLE_TYPE_ARTERIAL_WHOLE             0x5
#define DONGLE_TYPE_ARTERIAL_PLASMA            0x6
#define DONGLE_TYPE_UNDETER_WHOLE              0x7
#define DONGLE_TYPE_UNDETER_PLASMA             0x8
#define DONGLE_TYPE_ISF                        0x9
#define DONGLE_TYPE_CONTROL                    0xA

// Values for Location Nibble
#define DONGLE_LOCATION_FINGER                 0x10
#define DONGLE_LOCATION_AST                    0x20
#define DONGLE_LOCATION_EARLOBE                0x30
#define DONGLE_LOCATION_CONTROL                0x40
#define DONGLE_LOCATION_NOT_AVAIL              0xF0

// Values for Annunciation
#define DONGLE_ANNUNCIATION_BATT_LOW           0x0001
#define DONGLE_ANNUNCIATION_SENS_BAD           0x0002
#define DONGLE_ANNUNCIATION_SAMP_SIZE          0x0004
#define DONGLE_ANNUNCIATION_INSERT             0x0008
#define DONGLE_ANNUNCIATION_STRIP              0x0010
#define DONGLE_ANNUNCIATION_TOO_HIGH           0x0020
#define DONGLE_ANNUNCIATION_TOO_LOW            0x0040
#define DONGLE_ANNUNCIATION_TEMP_HIGH          0x0080
#define DONGLE_ANNUNCIATION_TEMP_LOW           0x0100
#define DONGLE_ANNUNCIATION_INTERRUPTED        0x0200
#define DONGLE_ANNUNCIATION_GENERAL            0x0400
#define DONGLE_ANNUNCIATION_TIME               0x0800

// Values for context flags
#define DONGLE_CONTEXT_FLAG_CARBO              0x01
#define DONGLE_CONTEXT_FLAG_MEAL               0x02
#define DONGLE_CONTEXT_FLAG_TESTER_HEALTH      0x04
#define DONGLE_CONTEXT_FLAG_EXERCISE           0x08
#define DONGLE_CONTEXT_FLAG_MEDICATION         0x10
#define DONGLE_CONTEXT_FLAG_MEDICATION_UNITS   0x20
#define DONGLE_CONTEXT_FLAG_HbA1c              0x40
#define DONGLE_CONTEXT_FLAG_EXTENDED           0x80

//We leave the extended flags out of this all, since there aren't any in the current spec  
#define DONGLE_CONTEXT_FLAG_ALL  (DONGLE_CONTEXT_FLAG_CARBO | DONGLE_CONTEXT_FLAG_MEAL | DONGLE_CONTEXT_FLAG_TESTER_HEALTH | DONGLE_CONTEXT_FLAG_EXERCISE | DONGLE_CONTEXT_FLAG_MEDICATION | DONGLE_CONTEXT_FLAG_MEDICATION_UNITS | DONGLE_CONTEXT_FLAG_HbA1c)
  
// Values for Carbohydratea
#define DONGLE_CARBO_BREAKFAST                 0x01
#define DONGLE_CARBO_LUNCH                     0x02
#define DONGLE_CARBO_DINNER                    0x03
#define DONGLE_CARBO_SNACK                     0x04
#define DONGLE_CARBO_DRINK                     0x05
#define DONGLE_CARBO_SUPPER                    0x06
#define DONGLE_CARBO_BRUNCH                    0x07

// Values for Meal
#define DONGLE_MEAL_PREPRANDIAL                0x1
#define DONGLE_MEAL_POSTPRANDIAL               0x2
#define DONGLE_MEAL_FASTING                    0x3
#define DONGLE_MEAL_CASUAL                     0x4

// Values for Tester Nibble
#define DONGLE_TESTER_SELF                     0x1
#define DONGLE_TESTER_PROFESSIONAL             0x2
#define DONGLE_TESTER_LAB_TEST                 0x3
#define DONGLE_TESTER_UNAVAILABLE              0xF

// Values for Health Nibble
#define DONGLE_HEALTH_MINOR                    0x10
#define DONGLE_HEALTH_MAJOR                    0x20
#define DONGLE_HEALTH_MENSES                   0x30
#define DONGLE_HEALTH_STRESS                   0x40
#define DONGLE_HEALTH_NONE                     0x50
#define DONGLE_HEALTH_UNAVAILABLE              0xF0

// Values for Medication ID
#define DONGLE_MEDICATION_RAPID                0x1
#define DONGLE_MEDICATION_SHORT                0x2
#define DONGLE_MEDICATION_INTERMEDIATE         0x3
#define DONGLE_MEDICATION_LONG                 0x4
#define DONGLE_MEDICATION_PRE_MIXED            0x5

// Values for Dongle Features
#define DONGLE_FEAT_LOW_BAT_SUP                0x0001
#define DONGLE_FEAT_MALFUNC_SUP                0x0002
#define DONGLE_FEAT_SAMPLE_SIZE_SUP            0x0004
#define DONGLE_FEAT_INSERT_ERR_SUP             0x0008
#define DONGLE_FEAT_TYPE_ERR_SUP               0x0010
#define DONGLE_FEAT_RESULT_DET_SUP             0x0020
#define DONGLE_FEAT_TEMP_DET_SUP               0x0040
#define DONGLE_FEAT_READ_INT_SUP               0x0080
#define DONGLE_FEAT_GENERAL_FAULT_SUP          0x0100
#define DONGLE_FEAT_TIME_FAULT_SUP             0x0200
#define DONGLE_FEAT_MULTIPLE_BOND_SUP          0x0400

#define DONGLE_FEAT_ALL    (DONGLE_FEAT_LOW_BAT_SUP | DONGLE_FEAT_MALFUNC_SUP | DONGLE_FEAT_SAMPLE_SIZE_SUP | \
                             DONGLE_FEAT_INSERT_ERR_SUP | DONGLE_FEAT_TYPE_ERR_SUP | DONGLE_FEAT_RESULT_DET_SUP | \
                             DONGLE_FEAT_TEMP_DET_SUP | DONGLE_FEAT_READ_INT_SUP | DONGLE_FEAT_GENERAL_FAULT_SUP | \
                             DONGLE_FEAT_TIME_FAULT_SUP | DONGLE_FEAT_MULTIPLE_BOND_SUP)

// Dongle Service bit fields
#define DONGLE_SERVICE                      0x00000001

//Record Control Point values
#define CTL_PNT_OP_REQ_RSP                   0x06
#define CTL_PNT_OP_WRITE                     0x07
#define CTL_PNT_OP_READ                      0x08
#define CTL_PNT_OP_CONFIG                    0x09  
#define CTL_PNT_OP_WRITE_RSP                 0x0a
#define CTL_PNT_OP_READ_RSP                  0x0b
#define CTL_PNT_OP_CONFIG_RSP                0x0c  
#define CTL_PNT_OP_LED                       0x0d
#define CTL_PNT_NV_WRITE                     0x0e
#define CTL_PNT_NV_READ                      0x0f
#define CTL_PNT_OP_TIMER                     0x10

//Record Control Point operator
#define CTL_PNT_OPER_NULL                    0x00
  
//Record Control Point Response Codes
#define CTL_PNT_RSP_SUCCESS                  0x01
#define CTL_PNT_RSP_OPCODE_NOT_SUPPORTED     0x02
#define CTL_PNT_RSP_OPER_INVALID             0x03
#define CTL_PNT_RSP_OPER_NOT_SUPPORTED       0x04
#define CTL_PNT_RSP_OPERAND_INVALID          0x05
#define CTL_PNT_RSP_NO_RECORDS               0x06
#define CTL_PNT_RSP_ABORT_FAILED             0x07
#define CTL_PNT_RSP_PROC_NOT_CMPL            0x08

// Callback events
#define DONGLE_MEAS_NTF_ENABLED              1
#define DONGLE_MEAS_NTF_DISABLED             2
#define DONGLE_CONTEXT_NTF_ENABLED           3
#define DONGLE_CONTEXT_NTF_DISABLED          4    
#define DONGLE_CTL_PNT_IND_ENABLED           5
#define DONGLE_CTL_PNT_IND_DISABLED          6    
#define DONGLE_CTL_PNT_CMD                   7

// Procedure timeout in ms
#define DONGLE_PROCEDURE_TIMEOUT             30000

// ATT status values
#define DONGLE_ERR_IN_PROGRESS               0x80
#define DONGLE_ERR_CCC_CONFIG                0x81

/*********************************************************************
 * TYPEDEFS
 */

// Dongle Service callback function
typedef void (*dongleServiceCB_t)(uint8 event, uint8* data, uint8 dataLen);

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

/*********************************************************************
 * API FUNCTIONS 
 */

/*
 * Dongle_AddService- Initializes the Dongle service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */

extern bStatus_t Dongle_AddService( uint32 services );

/*
 * Dongle_Register - Register a callback function with the
 *          Dongle Service
 *
 * @param   pfnServiceCB - Callback function.
 */

extern void Dongle_Register( dongleServiceCB_t pfnServiceCB);

/*
 * Dongle_SetParameter - Set a Dongle parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t Dongle_SetParameter( uint8 param, uint8 len, void *value );
  
/*
 * Dongle_GetParameter - Get a Dongle parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t Dongle_GetParameter( uint8 param, void *value );

/*********************************************************************
 * @fn          Dongle_MeasSend
 *
 * @brief       Send a dongle measurement.
 *
 * @param       connHandle - connection handle
 * @param       pNoti - pointer to notification structure
 *
 * @return      Success or Failure
 */
extern bStatus_t Dongle_MeasSend( uint16 connHandle, attHandleValueNoti_t *pNoti, uint8 taskId );

/*********************************************************************
 * @fn          Dongle_ContextSend
 *
 * @brief       Send a dongle measurement context.
 *
 * @param       connHandle - connection handle
 * @param       pNoti - pointer to notification structure
 *
 * @return      Success or Failure
 */
extern bStatus_t Dongle_ContextSend( uint16 connHandle, attHandleValueNoti_t *pNoti, uint8 taskId );

/*********************************************************************
 * @fn          Dongle_CtlPntIndicate
 *
 * @brief       Send an indication containing a dongle
 *              measurement.
 *
 * @param       connHandle - connection handle
 * @param       pInd - pointer to indication structure
 *
 * @return      Success or Failure
 */
extern bStatus_t Dongle_CtlPntIndicate( uint16 connHandle, attHandleValueInd_t *pInd, uint8 taskId );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* DONGLESERVICE_H */
