/**************************************************************************************************
  Filename:       connectedPulseService.c
  Revised:        $Date $
  Revision:       $Revision $

  Description:    This file contains the Dongle sample service
                  for use with the Dongle   sample application.

 Copyright 2011-2013 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"
#include "connectedPulseService.h"
#include "hal_lcd.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

// Position of dongle measurement value in attribute array
#define DONGLE_MEAS_VALUE_POS         2
#define DONGLE_MEAS_CONFIG_POS        3
#define DONGLE_CONTEXT_VALUE_POS      5
#define DONGLE_CONTEXT_CONFIG_POS     6
#define DONGLE_CTL_PNT_VALUE_POS      10
#define DONGLE_CTL_PNT_CONFIG_POS     11

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// Dongle service
CONST uint8 dongleServUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(DONGLE_SERV_UUID), HI_UINT16(DONGLE_SERV_UUID)
};

// Dongle characteristic
CONST uint8 dongleMeasUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(DONGLE_MEAS_UUID), HI_UINT16(DONGLE_MEAS_UUID)
};

// Dongle Measurement Context
CONST uint8 dongleContextUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(DONGLE_CONTEXT_UUID), HI_UINT16(DONGLE_CONTEXT_UUID)
};

// Dongle Feature
CONST uint8 dongleFeatureUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(DONGLE_FEATURE_UUID), HI_UINT16(DONGLE_FEATURE_UUID)
};

// Record Control Point
CONST uint8 recordControlPointUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(RECORD_CONTROL_POINT_UUID), HI_UINT16(RECORD_CONTROL_POINT_UUID)
};

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/* TRUE if record transfer in progress */
extern bool dongleSendAllRecords;

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static dongleServiceCB_t dongleServiceCB;

/*********************************************************************
 * Profile Attributes - variables
 */

// Dongle Service attribute
static CONST gattAttrType_t dongleService = { ATT_BT_UUID_SIZE, dongleServUUID };

// Dongle Characteristic
static uint8 dongleProps = GATT_PROP_NOTIFY;
static gattCharCfg_t dongleMeasConfig[GATT_MAX_NUM_CONN];
static uint8 dongleMeas = 0;

// Measurement Context
static uint8  dongleContextProps = GATT_PROP_NOTIFY;
static uint8  dongleContext=0;
static gattCharCfg_t dongleContextConfig[GATT_MAX_NUM_CONN];

// Dongle Feature
static uint8 dongleFeatureProps = GATT_PROP_READ;
static uint16 dongleFeature = DONGLE_FEAT_ALL;

// Dongle Control
static uint8  dongleControlProps = GATT_PROP_INDICATE | GATT_PROP_WRITE;
static uint8  dongleControl=0;
static gattCharCfg_t dongleControlConfig[GATT_MAX_NUM_CONN];

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t dongleAttrTbl[] =
{
  // Dongle Service
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8 *)&dongleService                /* pValue */
  },

    // 1. Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &dongleProps
    },

    // 2. Characteristic Value
    {
      { ATT_BT_UUID_SIZE, dongleMeasUUID },
      0, //return READ_NOT_PERMITTED
      0,
      &dongleMeas
    },

    // 3.Characteristic Configuration
    {
      { ATT_BT_UUID_SIZE, clientCharCfgUUID },
      GATT_PERMIT_READ | GATT_PERMIT_WRITE,
      0,
      (uint8 *)&dongleMeasConfig
    },

    //////////////////////////////////////////////
    // MEASUREMENT CONTEXT
    //////////////////////////////////////////////

    // 4.Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &dongleContextProps
    },

    // 5.Characteristic Value
    {
      { ATT_BT_UUID_SIZE, dongleContextUUID },
      0, //return READ_NOT_PERMITTED
      0,
      &dongleContext
    },

    // 6.Characteristic Configuration
    {
      { ATT_BT_UUID_SIZE, clientCharCfgUUID },
      GATT_PERMIT_READ | GATT_PERMIT_WRITE,
      0,
      (uint8 *)&dongleContextConfig
    },

    //////////////////////////////////////////////
    // DONGLE FEATURE
    //////////////////////////////////////////////

    // 7.Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &dongleFeatureProps
    },

    // 8.Characteristic Value
    {
      { ATT_BT_UUID_SIZE, dongleFeatureUUID },
      GATT_PERMIT_READ,
      0,
      (uint8 *) &dongleFeature
    },

    //////////////////////////////////////////////
    // DONGLE CONTROL POINT
    //////////////////////////////////////////////

    // 9.Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &dongleControlProps
    },

    // 10.Characteristic Value
    {
      { ATT_BT_UUID_SIZE, recordControlPointUUID },
      GATT_PERMIT_WRITE /* | GATT_PERMIT_AUTHEN_WRITE */,
      0,
      &dongleControl
    },

    // 11.Characteristic Configuration
    {
      { ATT_BT_UUID_SIZE, clientCharCfgUUID },
      GATT_PERMIT_READ | GATT_PERMIT_WRITE,
      0,
      (uint8 *)&dongleControlConfig
    }
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 dongle_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                            uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen );
static bStatus_t dongle_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                 uint8 *pValue, uint8 len, uint16 offset );

static void dongle_HandleConnStatusCB( uint16 connHandle, uint8 changeType );

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Service Callbacks
CONST gattServiceCBs_t  dongleCBs =
{
  dongle_ReadAttrCB,   // Read callback function pointer
  dongle_WriteAttrCB,  // Write callback function pointer
  NULL                  // Authorization callback function pointer
};


/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      Dongle_AddService
 *
 * @brief   Initializes the Dongle   service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t Dongle_AddService( uint32 services )
{
  uint8 status = SUCCESS;

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, dongleMeasConfig );
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, dongleContextConfig );
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, dongleControlConfig );

  // Register with Link DB to receive link status change callback
  VOID linkDB_Register( dongle_HandleConnStatusCB );

  if ( services & DONGLE_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( dongleAttrTbl, GATT_NUM_ATTRS( dongleAttrTbl ),
                                          &dongleCBs );
  }
  return ( status );
}

/*********************************************************************
 * @fn      Dongle_Register
 *
 * @brief   Register a callback function with the Dongle Service.
 *
 * @param   pfnServiceCB - Callback function.
 *          pfnCtlPntCB - Callback for control point
 *
 * @return  None.
 */
extern void Dongle_Register( dongleServiceCB_t pfnServiceCB)
{
  dongleServiceCB = pfnServiceCB;
}

/*********************************************************************
 * @fn      Dongle_SetParameter
 *
 * @brief   Set a dongle parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Dongle_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case DONGLE_FEATURE_PARAM:
      dongleFeature = *((uint16*)value);
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn      Dongle_GetParameter
 *
 * @brief   Get a Dongle parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to get.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Dongle_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case DONGLE_FEATURE_PARAM:
      *((uint16*)value) = dongleFeature;
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn          Dongle_MeasSend
 *
 * @brief       Send a dongle measurement.
 *
 * @param       connHandle - connection handle
 * @param       pNoti - pointer to notification structure
 *
 * @return      Success or Failure
 */
bStatus_t Dongle_MeasSend( uint16 connHandle, attHandleValueNoti_t *pNoti, uint8 taskId )
{
  uint16 value = GATTServApp_ReadCharCfg( connHandle, dongleMeasConfig );

  // If notifications enabled
  if ( value & GATT_CLIENT_CFG_NOTIFY )
  {
    // Set the handle
    pNoti->handle = dongleAttrTbl[DONGLE_MEAS_VALUE_POS].handle;

#if (HAL_LCD == TRUE)
    // HalLcdWriteString("Dongle_MeasSend", 1 );
#endif
    // Send the Indication
    return GATT_Notification( connHandle, pNoti, FALSE );
  }

  return bleNotReady;
}


/*********************************************************************
 * @fn          Dongle_ContextSend
 *
 * @brief       Send a dongle measurement context.
 *
 * @param       connHandle - connection handle
 * @param       pNoti - pointer to notification structure
 *
 * @return      Success or Failure
 */
bStatus_t Dongle_ContextSend( uint16 connHandle, attHandleValueNoti_t *pNoti, uint8 taskId )
{
  uint16 value = GATTServApp_ReadCharCfg( connHandle, dongleContextConfig );

  // If notifications enabled
  if ( value & GATT_CLIENT_CFG_NOTIFY )
  {
    // Set the handle
    pNoti->handle = dongleAttrTbl[DONGLE_CONTEXT_VALUE_POS].handle;

    // Send the Indication
    return GATT_Notification( connHandle, pNoti, FALSE );
  }

  return bleNotReady;
}

/*********************************************************************
 * @fn          Dongle_CtlPntIndicate
 *
 * @brief       Send an indication containing a control point
 *              message.
 *
 * @param       connHandle - connection handle
 * @param       pInd - pointer to indication structure
 *
 * @return      Success or Failure
 */
bStatus_t Dongle_CtlPntIndicate( uint16 connHandle, attHandleValueInd_t *pInd, uint8 taskId )
{
  uint16 value = GATTServApp_ReadCharCfg( connHandle, dongleControlConfig );

  // If indications enabled
  if ( value & GATT_CLIENT_CFG_INDICATE )
  {
    // Set the handle
    pInd->handle = dongleAttrTbl[DONGLE_CTL_PNT_VALUE_POS].handle;

    // Send the Indication
    return GATT_Indication( connHandle, pInd, FALSE, taskId );
  }

  return bleNotReady;
}

/*********************************************************************
 * @fn          dongle_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 *
 * @return      Success or Failure
 */
static uint8 dongle_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                            uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen )
{
  bStatus_t status = SUCCESS;

  // Require security for all characteristics
  if ( linkDB_Encrypted( connHandle ) == FALSE )
  {
    return ATT_ERR_INSUFFICIENT_ENCRYPT;
  }

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those types for reads

      case DONGLE_FEATURE_UUID:
        *pLen = 2;
        pValue[0] = LO_UINT16( dongleFeature );
        pValue[1] = HI_UINT16( dongleFeature );
        break;

      default:
        // Should never get here! (characteristics 3 and 4 do not have read permissions)
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }

  return ( status );
}

/*********************************************************************
 * @fn      dongle_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 *
 * @return  Success or Failure
 */
static bStatus_t dongle_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                 uint8 *pValue, uint8 len, uint16 offset )
{
  bStatus_t status = SUCCESS;

  // Require security for all characteristics
  if ( linkDB_Encrypted( connHandle ) == FALSE )
  {
    return ATT_ERR_INSUFFICIENT_ENCRYPT;
  }

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);

  switch ( uuid )
  {

  case  GATT_CLIENT_CHAR_CFG_UUID:
      // Dongle Notifications
      if ((pAttr->handle == dongleAttrTbl[DONGLE_MEAS_CONFIG_POS].handle ||
           pAttr->handle == dongleAttrTbl[DONGLE_CONTEXT_CONFIG_POS].handle))
      {
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        if ( status == SUCCESS )
        {
          uint16 charCfg = BUILD_UINT16( pValue[0], pValue[1] );

          if(pAttr->handle == dongleAttrTbl[DONGLE_MEAS_CONFIG_POS].handle)
          {
            (*dongleServiceCB)((charCfg == 0) ? DONGLE_MEAS_NTF_DISABLED :
                                                 DONGLE_MEAS_NTF_ENABLED, NULL, NULL);
          }
          else
          {
            (*dongleServiceCB)((charCfg == 0) ? DONGLE_CONTEXT_NTF_DISABLED :
                                                 DONGLE_CONTEXT_NTF_ENABLED, NULL, NULL);
          }
        }
      }
      // Dongle Indications
      else if ( pAttr->handle == dongleAttrTbl[DONGLE_CTL_PNT_CONFIG_POS].handle )
      {
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_INDICATE );
        if ( status == SUCCESS )
        {
            uint16 charCfg = BUILD_UINT16( pValue[0], pValue[1] );

            (*dongleServiceCB)((charCfg == 0) ? DONGLE_CTL_PNT_IND_DISABLED :
                                                 DONGLE_CTL_PNT_IND_ENABLED, NULL, NULL);
        }
      }
      else
      {
        status = ATT_ERR_INVALID_VALUE;
      }
    break;

    case  RECORD_CONTROL_POINT_UUID:
#if (HAL_LCD == TRUE)      
      HalLcd_HW_WriteLineValue(3, pValue, len, 16);
#endif      
      if(len >= DONGLE_CTL_PNT_MIN_SIZE  && len <= DONGLE_CTL_PNT_MAX_SIZE)
      {
        (*dongleServiceCB)(DONGLE_CTL_PNT_CMD, pValue, len);
      }
      else
      {
        status = ATT_ERR_INVALID_VALUE_SIZE;
      }
    break;

    default:
      status = ATT_ERR_ATTR_NOT_FOUND;
      break;
  }

  return ( status );
}

/*********************************************************************
 * @fn          dongle_HandleConnStatusCB
 *
 * @brief       Simple Profile link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void dongle_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{
  // Make sure this is not loopback connection
  if ( connHandle != LOOPBACK_CONNHANDLE )
  {
    // Reset Client Char Config if connection has dropped
    if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
         ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) &&
           ( !linkDB_Up( connHandle ) ) ) )
    {
      GATTServApp_InitCharCfg( connHandle, dongleMeasConfig );
      GATTServApp_InitCharCfg( connHandle, dongleContextConfig );
      GATTServApp_InitCharCfg( connHandle, dongleControlConfig );
    }
  }
}

/*********************************************************************
*********************************************************************/
